/*jshint jquery: true */
var gridHeight = 16;
var gridWidth = 16;

for (var i = 1; i <= gridHeight; i++) {
    $('.gridContainer').append("<div class='gridRow'></div>");
    for (var j = 1; j <= gridWidth; j++) {
        $('.gridContainer').children('div').last().append("<div class='gridCell'></div>");
    }
}

$('.gridRow').on('mouseenter', '.gridCell', function() {
    $(this).css({'background-color': '#000'});
});